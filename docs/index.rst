.. flow3r documentation master file, created by
   sphinx-quickstart on Sun Jun 11 19:43:11 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to flow3r's documentation!
==================================

.. toctree::
   :maxdepth: 1
   :caption: The badge:

   badge/hardware_specs.rst
   badge/assembly.rst
   badge/usage.rst
   badge/programming.rst
   badge/firmware.rst
   badge/firmware-development.rst
   badge/badge_link.rst
   badge/badge_net.rst
   badge/bl00mbox.rst
   badge/fonts.rst

.. toctree::
   :maxdepth: 1
   :caption: API:

   api/audio.rst
   api/badgelink.rst
   api/badgenet.rst
   api/captouch.rst
   api/ctx.rst
   api/leds.rst
   api/uos.rst
   api/sys_buttons.rst
   api/sys_display.rst
   api/sys_kernel.rst

.. toctree::
   :maxdepth: 1
   :caption: About:

   badge/license.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
